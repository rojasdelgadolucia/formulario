package com.example.formulario

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import com.example.formulario.ui.theme.FormularioTheme
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class RegistroActivity : ComponentActivity(){
    private lateinit var firebaseAuth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()
        setContent {
            FormularioTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    RegistroView()
                }
            }
        }
    }
    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun RegistroView() {
        var email by remember {
            mutableStateOf("")
        }
        var password by remember {
            mutableStateOf("")
        }
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TextField(
                value = email,
                onValueChange = { email = it },//que valor va a tener y que pasa cuando cambia
                label = { Text(text = "Email") },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Email
                )
            )
            TextField(
                value = password,
                onValueChange = { password = it },//que valor va a tener y que pasa cuando cambia
                label = { Text(text = "Password") },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Password,
                    imeAction = ImeAction.Done
                )
            )
            Button(onClick = { registro(email, password) }) {
                Text(text = "Registrarse")
            }
        }
    }

    fun registro(email: String, password : String){
        if(email.isNotEmpty() && password.isNotEmpty() ){
            firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this){
                val user: FirebaseUser = firebaseAuth.currentUser!!
                verifyEmail(user)
                startActivity(Intent(this,flor::class.java))
            }.addOnFailureListener {
                Toast.makeText(this, "Error de autenticación", Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(this, "Inserta los datos bien", Toast.LENGTH_SHORT).show()
        }
    }
    fun verifyEmail(user: FirebaseUser){
        user.sendEmailVerification().addOnCompleteListener(this){
                task->
            if(task.isSuccessful){
                Toast.makeText(this, "Email verificado", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "Error al verificar email", Toast.LENGTH_SHORT).show()
            }
        }
    }


    @Preview(showBackground = true)
    @Composable
    fun GreetingPreview2() {
        FormularioTheme {
            RegistroView()
        }
    }
}